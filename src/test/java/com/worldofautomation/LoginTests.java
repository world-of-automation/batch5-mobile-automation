package com.worldofautomation;

import com.worldofautomation.pages.HomePage;
import com.worldofautomation.pages.LoginPage;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

public class LoginTests extends TestBase {


    @Test
    public void validateUserCantLoginWithNoPassword() {
        HomePage homePage = PageFactory.initElements(appiumDriver, HomePage.class);
        LoginPage loginPage = PageFactory.initElements(appiumDriver, LoginPage.class);

        homePage.clickOnAccount();
        loginPage.typeOnUsernameField();
        sleepFor(5);
    }

    @Test
    public void validateUserCantSwipeLeft() {
        HomePage homePage = PageFactory.initElements(appiumDriver, HomePage.class);
        homePage.swipeLeft();
        sleepFor(10);
    }
}
