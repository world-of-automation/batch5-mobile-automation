package com.worldofautomation.pages;

import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;

public class LoginPage {
    @AndroidFindBy(xpath = "//android.widget.EditText[@text='User name']")
    private WebElement usernameField;

    public void typeOnUsernameField() {
        usernameField.sendKeys("TestData");
    }
}
