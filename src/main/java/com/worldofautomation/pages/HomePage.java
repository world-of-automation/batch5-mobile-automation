package com.worldofautomation.pages;

import com.worldofautomation.TestBase;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.WebElement;

public class HomePage {

    @AndroidFindBy(xpath = "//android.widget.Button[@text='Accounts']")
    @iOSXCUITFindBy(xpath = "//android.widget.Button[@text='IOS-Accounts']")
    private WebElement accountButton;


    @AndroidFindBy(xpath = "//android.widget.Button[@text='Send Money']")
    private WebElement sendMoney;


    public void swipeLeft() {
        TestBase.swipeLeftOnElement(sendMoney);
    }

    public void clickOnAccount() {
        accountButton.click();
    }
}
