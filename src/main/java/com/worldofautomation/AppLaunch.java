package com.worldofautomation;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;

public class AppLaunch {

    public static void main(String[] args) throws MalformedURLException {

        DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
        desiredCapabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "6");
        desiredCapabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
        desiredCapabilities.setCapability(MobileCapabilityType.UDID, "9832f391");
        desiredCapabilities.setCapability(MobileCapabilityType.APP_PACKAGE, "com.tdbank");
        desiredCapabilities.setCapability(MobileCapabilityType.APP_ACTIVITY, "com.td.dcts.android.us.app.SplashScreenActivity");
        //desiredCapabilities.setCapability(MobileCapabilityType.APP,"src/main/resources/TD.apk");// may not work

        AppiumDriver driver = new AndroidDriver(new URL("http://localhost:4723/wd/hub"), desiredCapabilities);

        driver.getPageSource();


    }

}
