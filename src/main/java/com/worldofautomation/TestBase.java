package com.worldofautomation;


import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.LogStatus;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.remote.MobilePlatform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class TestBase {

    public static AppiumDriver appiumDriver;
    private static ExtentReports extent;

    @Parameters({"platformName", "platformVersion", "udid", "deviceName", "automationName", "appPackage", "appActivity", "bundleId"})
    @BeforeMethod
    public static void setAppiumDriver(String platformName, String platformVersion, String udid, String deviceName, String automationName, String appPackage, String appActivity, String bundleId) throws MalformedURLException {
        DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
        desiredCapabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, platformVersion);
        desiredCapabilities.setCapability(MobileCapabilityType.UDID, udid);
        if (platformName.equalsIgnoreCase("ios")) {
            desiredCapabilities.setCapability(MobileCapabilityType.DEVICE_NAME, deviceName);
            desiredCapabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, MobilePlatform.IOS);
            desiredCapabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, automationName);
            desiredCapabilities.setCapability("bundleId", bundleId);
            appiumDriver = new IOSDriver(new URL("http://localhost:4723/wd/hub"), desiredCapabilities);
        } else {
            desiredCapabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, MobilePlatform.ANDROID);
            desiredCapabilities.setCapability("appPackage", appPackage);
            desiredCapabilities.setCapability("appActivity", appActivity);
            appiumDriver = new AndroidDriver(new URL("http://localhost:4723/wd/hub"), desiredCapabilities);
        }
        appiumDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

    // JAVA CLIENT : 3.1.0
    /* *//**
     * This method will swipe either up, Down, left or Right according to the
     * direction specified. This method takes the size of the screen and uses
     * the swipe function present in the Appium driver to swipe on the screen
     * with a particular timeout. There is one more method to implement swipe
     * using touch actions, which is not put up here.
     *
     * @param direction       The direction we need to swipe in.
     * @param durationOfSwipe The swipe time, ie the time for which the driver is supposed
     *                        to swipe.
     * @param offset          The offset for the driver, eg. If you want to swipe 'up', then
     *                        the offset is the number of pixels you want to leave from the
     *                        bottom of the screen t start the swipe.
     * @Author -
     * @Modified By -
     *//*

    public static void functionSwipe(String direction, int durationOfSwipe, int offset) {
        Dimension size = appiumDriver.manage().window().getSize();
        int starty = (int) (size.height * 0.80);
        int endy = (int) (size.height * 0.20);
        int startx = size.width / 2;
        if (direction.equalsIgnoreCase("Up")) {
            ((AppiumDriver<WebElement>) (appiumDriver)).swipe(startx / 2, starty - offset, startx / 2, endy, durationOfSwipe);
        } else if (direction.equalsIgnoreCase("Down")) {
            ((AppiumDriver<WebElement>) (appiumDriver)).swipe(startx / 2, endy + offset, startx / 2, starty, durationOfSwipe);
        } else if (direction.equalsIgnoreCase("Right")) {
            starty = size.height / 2;
            endy = size.height / 2;
            startx = (int) (size.width * 0.10);
            int endx = (int) (size.width * 0.90);
            ((AppiumDriver<WebElement>) (appiumDriver)).swipe(startx + offset, starty, endx, endy, durationOfSwipe);
        } else if (direction.equalsIgnoreCase("Left")) {
            starty = size.height / 2;
            endy = size.height / 2;
            startx = (int) (size.width * 0.90);
            int endx = (int) (size.width * 0.10);
            ((AppiumDriver<WebElement>) (appiumDriver)).swipe(startx - offset, starty, endx, endy, durationOfSwipe);
        }
    }

    */

    /**
     * This method will be responsible for swiping to the left on a specific element
     *
     * @param element WebElement on which you want to perform the swipe
     * @author - xyz
     * @modified by - abc
     *//*
    public static void swipeLeftOnElement(WebElement element) {
        Point startButtonPoint = element.getLocation();
        Dimension startButtonDimenstion = element.getSize();
        int y = startButtonPoint.getY() + startButtonDimenstion.getHeight() / 2;
        int xStart = startButtonPoint.getX() + (int) (startButtonDimenstion.getWidth() * 0.8);
        int xEnd = (startButtonPoint.getX() * 1);
        TestBase.appiumDriver.swipe(xStart, y, xEnd, y, 1000);
    }*/

    public static void functionSwipe(String direction, int durationOfSwipe, int offset) {
        //TO DO with 8.0.0
    }

    public static void swipeLeftOnElement(WebElement element) {
        //TO DO with 8.0.0
    }

    /**
     * This method will be responsible for setting up the extent report
     *
     * @param context
     */
    @BeforeSuite
    public void extentSetup(ITestContext context) {
        ExtentTestManager.setOutputDirectory(context);
        extent = ExtentTestManager.getInstance();
    }


    @BeforeMethod
    public void startExtent(Method method) {
        String className = method.getDeclaringClass().getSimpleName();
        ExtentTestManager.startTest(method.getName());
        ExtentTestManager.getTest().assignCategory(className);
    }

    @AfterMethod
    public void afterEachTestMethod(ITestResult result) {
        ExtentTestManager.getTest().getTest().setStartedTime(ExtentTestManager.getTime(result.getStartMillis()));
        ExtentTestManager.getTest().getTest().setEndedTime(ExtentTestManager.getTime(result.getEndMillis()));
        for (String group : result.getMethod().getGroups()) {
            ExtentTestManager.getTest().assignCategory(group);
        }

        if (result.getStatus() == 1) {
            ExtentTestManager.getTest().log(LogStatus.PASS, "TEST CASE PASSED : " + result.getName());
        } else if (result.getStatus() == 2) {
            ExtentTestManager.getTest().log(LogStatus.FAIL, "TEST CASE FAILED : " + result.getName() + " :: " + ExtentTestManager.getStackTrace(result.getThrowable()));
        } else if (result.getStatus() == 3) {
            ExtentTestManager.getTest().log(LogStatus.SKIP, "TEST CASE SKIPPED : " + result.getName());
        }
        ExtentTestManager.endTest();
        extent.flush();
        if (result.getStatus() == ITestResult.FAILURE) {
            ExtentTestManager.captureScreenshot(appiumDriver, result.getName());
        }
    }

    @AfterSuite
    public void generateReport() {
        extent.close();
    }
    // Extent report ends


    @AfterMethod
    public void cleanUp() {
        appiumDriver.quit();
    }

    public void sleepFor(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException ignored) {
        }
    }
}
